<?php
namespace Balance;

class EntityTest extends \PHPUnit_Framework_TestCase
{
    public function testEntityCreationGettersAndSetters()
    {
        $entity = new Entity(1);

        $this->assertInstanceOf(Entity::class, $entity);
        $this->assertEquals(1, $entity->getBalance());

        $entity->setBalance('12');
        $this->assertEquals(12, $entity->getBalance());
        $this->assertInternalType('float', $entity->getBalance());
    }
}
