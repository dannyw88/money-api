<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/../bootstrap.php';

$app = $di->get(Slim\App::class);

$app->add('Middleware\Validator');
$app->add('Middleware\DetermineRoute');

// Collection
$app->get('/balance/{user}', 'balance:getCollection');
$app->put('/balance/{user}', 'balance:putUser');
$app->delete('/balance/{user}', 'balance:deleteUser');

// Entity
$app->get('/balance/{user}/{bank}', 'balance:get');
$app->put('/balance/{user}/{bank}', 'balance:put');
$app->delete('/balance/{user}/{bank}', 'balance:delete');

$app->run();
