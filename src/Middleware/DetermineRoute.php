<?php
namespace Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use FastRoute\Dispatcher;
use Interop\Container\ContainerInterface;
use Slim\Router;

class DetermineRoute
{
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $router = $this->router;
        $routeInfo = $router->dispatch($request);

        if ($routeInfo[0] === Dispatcher::FOUND) {
            $routeArguments = [];
            foreach ($routeInfo[2] as $k => $v) {
                $routeArguments[$k] = urldecode($v);
            }
            $route = $router->lookupRoute($routeInfo[1]);
            $route->prepare($request, $routeArguments);
            $request = $request->withAttribute('route', $route);
        }

        $routeInfo['request'] = [$request->getMethod(), (string) $request->getUri()];
        $request = $request->withAttribute('routeInfo', $routeInfo);
        return $next($request, $response);
    }
}
