<?php
spl_autoload_register(function($classname) {
    $path = __DIR__ . '/src/' . str_replace('\\', '/', $classname) . '.php';
    if (file_exists($path)) {
        require_once $path;
    }
});
require_once __DIR__ . '/vendor/autoload.php';
