<?php
namespace Balance;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Service
{
    protected $repository;
    protected $mapper;

    public function __construct(RepositoryInterface $repository, Mapper $mapper)
    {
        $this->repository = $repository;
        $this->mapper = $mapper;
    }

    public function get(Request $request, Response $response): Response
    {
        try {
            $entity = $this->repository->get(
                $request->getAttribute('user'),
                $request->getAttribute('bank')
            );
        } catch (\OutOfBoundsException $e) {
            return $response->withStatus(404);
        }
        return $response->withJson($this->mapper->toArray($entity));
    }

    public function put(Request $request, Response $response): Response
    {
        try {
            $entity = $this->repository->put(
                $request->getAttribute('user'),
                $request->getAttribute('bank'),
                $request->getParsedBody()
            );
        } catch (\OutOfBoundsException $e) {
            return $response->withStatus(404);
        }
        return $response->withJson($this->mapper->toArray($entity));
    }

    public function delete(Request $request, Response $response): Response
    {
        try {
            $this->repository->delete(
                $request->getAttribute('user'),
                $request->getAttribute('bank')
            );
        } catch (\OutOfBoundsException $e) {
            return $response->withStatus(404);
        }
        return $response->withStatus(204);
    }
}
