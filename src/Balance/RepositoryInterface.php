<?php
namespace Balance;

interface RepositoryInterface
{
    /**
     * Get a Balance Entity
     * @throws \UnexpectedValueException
     */
    public function get(string $user, string $bank): Entity;
}
