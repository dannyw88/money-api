<?php
namespace Extension;

/**
 * Starts the built in PHP server if the PHPBrowser module is used in the suite
 */
class BuiltInServer extends \Codeception\Extension
{
    const DEFAULT_DOCROOT = 'public';
    const DEFAULT_TEST_PATH = '/';
    const DEFAULT_WAIT = 5000;
    const DEFAULT_RETRIES = 100;

    protected $pid;

    public static $events = [
        'suite.before' => 'beforeSuite'
    ];

    public function __construct($config, $options)
    {
        $config['docroot'] = $config['docroot'] ?? static::DEFAULT_DOCROOT;
        $config['testPath'] = $config['testPath'] ?? static::DEFAULT_TEST_PATH;
        $config['wait'] = $config['wait'] ?? static::DEFAULT_WAIT;
        $config['retries'] = $config['retries'] ?? static::DEFAULT_RETRIES;
        parent::__construct($config, $options);
    }

    public function beforeSuite()
    {
        if ($this->pid || ! $this->hasModule('PhpBrowser')) {
            return;
        }
        $phpBrowserConfig = $this->getModule('PhpBrowser')->_getConfig();

        $host = $phpBrowserConfig['url'];
        $port = $phpBrowserConfig['curl']['CURLOPT_PORT'] ?? null;
        $docroot = $this->config['docroot'];

        $this->pid = $this->connect($host, $port, $docroot);
        register_shutdown_function($this);
        $this->waitForResponse($host, $port, $this->config['testPath']);
    }

    protected function connect($host, $port, $docroot): int
    {
        $command = sprintf(
            'php -S %s:%d -t %s >/dev/null 2>&1 & echo $!',
            $host,
            $port,
            $docroot
        );

        exec($command, $output);
        $pid = (int) $output[0];
        $this->writeLn('<debug>Web server started on ' . $host . ':' . $port . ' with PID ' . $pid . '</debug>');
        return $pid;
    }

    protected function waitForResponse($host, $port, $path)
    {
        $handle = curl_init($host . $path);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_HEADER, 1);
        curl_setopt($handle, CURLOPT_NOBODY, 1);
        if ($port) {
            curl_setopt($handle, CURLOPT_PORT, $port);
        }

        $retries = $this->config['retries'];
        do {
            $connected = curl_exec($handle);
            usleep($this->config['wait']);
        } while (! $connected && --$retries);

        if (! $connected) {
            $this->killProcess();
            throw new \Exception('Could not get a response from the local server');
        }
    }

    public function killProcess()
    {
        if (! $this->pid) {
            return;
        }
        exec('kill ' . $this->pid);
        $this->writeLn('<debug>Killing web server process with PID ' . $this->pid . '</debug>');
        $this->pid = null;
    }

    public function __invoke()
    {
        $this->killProcess();
    }
}
