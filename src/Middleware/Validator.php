<?php
namespace Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Exceptions\NestedValidationException;
use Slim\Route;

class Validator
{
    protected $rules;

    public function __construct(array $rules = [])
    {
        $this->rules = $rules;
    }

    public function addRule(string $pattern, string $method, callable $callable): self
    {
        $this->rules[$pattern][$method] = $callable;
        return $this;
    }

    public function __invoke(Request $request, Response $response, callable $next): Response
    {
        $route = $request->getAttribute('route');
        if (! $route ||
           ! isset($this->rules[$route->getPattern()]) ||
           ! isset($this->rules[$route->getPattern()][$request->getMethod()]) ||
           ! is_callable($this->rules[$route->getPattern()][$request->getMethod()])
        ) {
            return $next($request, $response);
        }

        $validator = $this->rules[$route->getPattern()][$request->getMethod()]();

        try {
            $validator->assert($request->getParams());
        } catch (NestedValidationException $e) {
            return $response->withJson($e->getMessages(), 412);
        }

        return $next($request, $response);
    }
}
