<?php
namespace Middleware;

use Codeception\Util\Stub;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Route;

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
    protected $curl;

    public function __construct()
    {
    }

    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testValidatorReturnsResponseWhenNoConfigIsSet()
    {
        $config = [];

        $request = Stub::makeEmpty(Request::class);
        $response = Stub::makeEmpty(Response::class);
        $route = Stub::makeEmpty(Route::class);
        $next = function($request, $response) {
            return $response;
        };

        $validator = new Validator($config);
        $response = $validator($request, $response, $next);
        $this->assertInstanceOf(Response::class, $response);
    }
}
