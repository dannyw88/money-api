<?php
namespace Balance;

class Entity
{
    protected $balance;

    public function __construct(float $balance)
    {
        $this->setBalance($balance);
    }

    public function setBalance(float $balance): self
    {
        $this->balance = number_format($balance, 2);
        return $this;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }
}
