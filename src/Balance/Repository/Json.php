<?php
namespace Balance\Repository;

use Balance\Entity;
use Balance\Mapper;
use Balance\RepositoryInterface;

class Json implements RepositoryInterface
{
    const LOCATION = __DIR__ . '/../../../data/balance/';
    protected $mapper;

    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    public function get(string $user, string $bank): Entity
    {
        $filename = $this->getFilePath($user, $bank);
        if (! file_exists($filename)) {
            throw new \OutOfBoundsException($filename);
        }
        $array = json_decode(file_get_contents($filename), true);
        return $this->mapper->fromArray($array);
    }

    public function put(string $user, string $bank, array $args): Entity
    {
        $folder = $this->getFolderPath($user);
        $filename = $this->getFilePath($user, $bank);
        if (! file_exists($folder)) {
            mkdir($folder, 0777, true);
            // throw new \OutOfBoundsException($filename);
        }
        $entity = $this->mapper->fromArray($args);
        file_put_contents($filename, json_encode($this->mapper->toArray($entity)));
        return $entity;
    }

    public function delete(string $user, string $bank): bool
    {
        $filename = $this->getFilePath($user, $bank);
        if (! file_exists($filename)) {
            throw new \OutOfBoundsException($filename);
        }
        return unlink($filename);
    }

    protected function getFolderPath($user)
    {
        return static::LOCATION . $user;
    }

    protected function getFilePath($user, $bank)
    {
        return $this->getFolderPath($user) . '/' . $bank . '.json';
    }
}
