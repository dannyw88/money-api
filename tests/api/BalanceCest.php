<?php
class BalanceCest
{
    const FORMAT = [
        'balance' => 'float:!empty',
    ];

    public function checkEndpoints(ApiTester $i)
    {
        $i->wantTo('check only the correct endpoints are accessible');

        $i->sendGET('balance/danny/foo');
        $i->dontSeeResponseCodeIs(405);

        $i->sendPOST('balance/danny/foo');
        $i->seeResponseCodeIs(405);

        $i->sendPUT('balance/danny/foo');
        $i->dontSeeResponseCodeIs(405);

        $i->sendDELETE('balance/danny/foo');
        $i->dontSeeResponseCodeIs(405);

        $i->sendGET('balance/danny');
        $i->dontSeeResponseCodeIs(405);

        $i->sendPOST('balance/danny');
        $i->seeResponseCodeIs(405);

        $i->sendPUT('balance/danny');
        $i->dontSeeResponseCodeIs(405);

        $i->sendDELETE('balance/danny');
        $i->dontSeeResponseCodeIs(405);
    }

    public function testGetResponseReturns404WhenEmpty(ApiTester $i)
    {
        $i->wantTo('test we get a 404 when theres no data');
        $i->sendGET('balance/danny/halifax');
        $i->seeResponseCodeIs(404);
    }

    public function testPutResponse(ApiTester $i)
    {
        $i->wantTo('test we can put data to an endpoint');
        $data = ['balance' => 2.2];
        $i->sendPUT('balance/danny/halifax', $data);
        $i->seeResponseCodeIs(200);
        $i->seeResponseMatchesJsonType(static::FORMAT);
        $i->seeResponseContainsJson($data);
    }

    public function testPutResponseValidation(ApiTester $i)
    {
        $i->wantTo('test validation on put works as expected');
        $data = ['bal' => 2.2];
        $i->sendPUT('balance/danny/halifax', $data);
        $i->seeResponseCodeIs(412);
        $i->seeResponseContainsJson(['Key balance must be present']);
    }

    public function testGetWithData(ApiTester $i)
    {
        $i->wantTo('test we can get data');
        $data = ['balance' => 2.2];
        $i->sendGET('balance/danny/halifax');
        $i->seeResponseCodeIs(200);
        $i->seeResponseMatchesJsonType(static::FORMAT);
        $i->seeResponseContainsJson($data);
    }

    public function testPutResponseNewData(ApiTester $i)
    {
        $data = ['balance' => 2.9];
        $i->wantTo('test we can put data to a new endpoint');
        $i->sendPUT('balance/danny/capitalOne', $data);
        $i->seeResponseCodeIs(200);
        $i->seeResponseMatchesJsonType(static::FORMAT);
        $i->seeResponseContainsJson($data);
    }

    public function testDeleteData(ApiTester $i)
    {
        $i->wantTo('test we can delete data');

        $i->sendDelete('balance/danny/capitalOne');
        $i->seeResponseCodeIs(204);

        $i->sendGet('balance/danny/capitalOne');
        $i->seeResponseCodeIs(404);

        $i->sendDelete('balance/danny/capitalOne');
        $i->seeResponseCodeIs(404);
    }
}
