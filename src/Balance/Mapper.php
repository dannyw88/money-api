<?php
namespace Balance;

class Mapper
{
    public function fromArray(array $data): Entity
    {
        return new Entity(
            $data['balance']
        );
    }

    public function toArray(Entity $entity): array
    {
        return [
            'balance' => $entity->getBalance()
        ];
    }
}
