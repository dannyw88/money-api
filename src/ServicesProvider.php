<?php

use DD\DiMaria;
use Pimple\ServiceProviderInterface;
use Pimple\Container;

class ServicesProvider implements ServiceProviderInterface
{
    protected $di;
    protected $diMap = [
        'balance' => 'Balance\Service'
    ];

    public function __construct(DiMaria $di)
    {
        $this->di = $di;
    }

    public function register(Container $container)
    {
        $container['di'] = $this->di;

        $container['notFoundHandler'] = function ($container) {
            return function ($request, $response) use ($container) {
                return $response
                    ->withStatus(404)
                    ->withHeader('Content-Type', 'text/html')
                    ->write('Something went wrong!');
            };
        };

        foreach ($this->diMap as $value => $class) {
            $container[$value] = function($container) use ($class) {
                return $container['di']->get($class);
            };
        }
    }
}
