<?php
namespace Helper;

class Api extends \Codeception\Module
{
    const TEMP_DATA_LOCATION = '__data';

    public function _beforeSuite($settings = [])
    {
        $i = $this->getModule('Filesystem');
        $i->copyDir('data', static::TEMP_DATA_LOCATION);
        $i->cleanDir('data');
    }

    public function _afterSuite()
    {
        $i = $this->getModule('Filesystem');
        $i->cleanDir('data');
        $i->copyDir(static::TEMP_DATA_LOCATION, 'data');
        $i->deleteDir(static::TEMP_DATA_LOCATION);
    }
}
