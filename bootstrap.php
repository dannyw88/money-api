<?php
error_reporting(E_ALL);
ini_set('DISPLAY_ERRORS', 'stdout');

use Respect\Validation\Validator as v;

require_once __DIR__ . '/autoload.php';

$di = new DD\DiMaria;
$di
    ->set('settings', [
        'httpVersion' => '1.1',
        'responseChunkSize' => 4096,
        'outputBuffering' => 'append',
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,
    ])
    ->setAlias('environment', 'Slim\Http\Environment', ['items' => $_SERVER])
    ->setFactory('request', function() use ($di) {
        return Slim\Http\Request::createFromEnvironment($di->get('environment'));
    })
    ->setFactory('response', function() use ($di) {
        $headers = new Slim\Http\Headers(['Content-Type' => 'text/html; charset=UTF-8']);
        $response = new Slim\Http\Response(200, $headers);
        return $response->withProtocolVersion($di->get('settings')['httpVersion']);
    })
    ->setAlias('router', Slim\Router::class)
    ->setAlias('foundHandler', Slim\Handlers\Strategies\RequestResponse::class)
    ->setAlias('phpErrorHandler', Slim\Handlers\PhpError::class, [
        'displayErrorDetails' => $di->get('settings')['displayErrorDetails']
    ])
    ->setAlias('errorHandler', Slim\Handlers\Error::class, [
        'displayErrorDetails' => $di->get('settings')['displayErrorDetails']
    ])
    ->setAlias('notFoundHandler', Slim\Handlers\NotFound::class)
    ->setAlias('notAllowedHandler', Slim\Handlers\NotAllowed::class)
    ->setAlias('callableResolver', Slim\CallableResolver::class, ['container' => $di])
    ->setShared('request', false)
    ->setShared('response', false)
    ->setShared('router', true)
    ->setParams(Slim\App::class, ['container' => $di]);

$di->setParams(Middleware\DetermineRoute::class, ['router' => ['instanceOf' => 'router']]);
$di->setParams(Middleware\Validator::class, [
    'rules' => [
        '/balance/{user}/{bank}' => [
            'PUT' => function() { return v::key('balance', v::floatVal()); }
        ]
    ]
]);

$di->setAlias('balance', Balance\Service::class);
$di->setPreference(Balance\RepositoryInterface::class, Balance\Repository\Json::class);


// if (PHP_SAPI == 'cli-server') {
//     // To help the built-in PHP dev server, check if the request was actually for
//     // something which should probably be served as a static file
//     $file = __DIR__ . $_SERVER['REQUEST_URI'];
//     if (is_file($file)) {
//         return false;
//     }
// }

// regexDiMaria = new RegExp("\\#{element}[\\s]*=[\\s]*\\$di->get\\([\"\']*?([A-Z][a-zA-Z_\\\\]*)+", "g")
