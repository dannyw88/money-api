<?php
namespace Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * A Wrapper to create middleware classes only when the route is invoked
 * If a middleware is only used for particular routes/groups then this saves
 * creating the object and fetching its dependencies unneccesarily.
 *
 * Also passes the container to the middleware, as closures are binded to the Container
 * but middleware classes don't have access to this.
 */
class Factory
{
    public function __invoke(string $middleware, array $params = []): callable
    {
        return function (Request $request, Response $response, callable $next) use ($middleware, $params) {
            $params['container'] = $this;
            return $this->di->get($middleware, $params)($request, $response, $next);
        };
    }
}
